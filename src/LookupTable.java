import java.util.LinkedList;
import java.util.List;

public class LookupTable {

  private List<Token> tokens;

  public LookupTable() {
    tokens = new LinkedList<Token>();
  }

  public List<Token> getTokens() {
    return tokens;
  }

  public void addToken(Token token) {
    tokens.add(token);
  }

  public List<Token> getTokensByType(TokenType type) {
    List<Token> tokenList = new LinkedList<Token>();
    for (Token token : tokens) {
      if (token.type == type) {
        tokenList.add(token);
      }
    }
    return tokenList;
  }
}