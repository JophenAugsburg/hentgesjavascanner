public enum TokenType {
  COMMENT, // a comment
  WHITESPACE, // whitespace

  // data types
  DATATYPE_STRING,
  DATATYPE_INT,
  DATATYPE_FLOAT,
  DATATYPE_CHAR,
  DATATYPE_BOOLEAN,

  VARIABLE, // any left of characters & numbers with a letter

  STRING, // ex: apple
  CHAR, // ex: a
  INT, // 1, 2, 3, 4, ...
  FLOAT, // 1.0, 1.23, ...

  IF, // if
  ELSE, // else

  WHILE, // while
  FOR, // while

  RETURN, // return
  THIS, // this or this.<variable>
  OVERRIDE, // @Override

  PLUS, // +
  MINUS, // -
  MULTIPLY, // *
  DIVIDE, // \

  SEMICOLON, // ;
  EQUALS, // =
  LEFTPARENTHESIS, // (
  RIGHTPARENTHESIS, // )
  ATSIGN, // @
  LEFTCURLYBRACKET, // {
  RIGHTCURLYBRACKET, // }
  COMMA, // ,
}