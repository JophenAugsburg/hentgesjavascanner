public class Token
{
  private static int IDCOUNT = 0;
  int ID;
  TokenType type;
  String lexeme;

  public Token(TokenType type, String lexeme) {
    ID = IDCOUNT;
    this.type = type;
    this.lexeme = lexeme;
    IDCOUNT += 1;
  }

  @Override
  public String toString() {
    return "ID: " + ID + " | Type: " + type + " | Lexeme: " + lexeme;
  }
}