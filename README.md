# Java Scanner - Using JFlex
A custom Java scanner, created with the program, JFlex.

## Features

**Libraries**
* [x] **[JFlex](https://www.jflex.de/)** v1.7.0

## Usage

Generate Scanner From JFlex
```
$ java -jar jflex.jar ScannerSchema.flex
```

Compile and Run The Scanner
```bash
$ mv MyScanner.java src
$ javac -d bin src/*.java
$ java -cp bin Main temp.txt
```

Combine All Commands
```bash
$ java -jar jflex.jar ScannerSchema.flex; mv MyScanner.java src; javac -d bin src/*.java; java -cp bin Main temp.txt
```

## Directory structure
```txt
+---README.md
+---Standard Design Document.docx
+---Standard Design Document.pdf
```
