// ScannerSchema.flex
// Joseph Hentges

/**
 * This is the JFlex defininition for the scanner.
 * The schema that is used in JFlex for creating a custom scanner.
 *
 * @author Joseph Hentges
 * December 10, 2019
 * Augsburg University
 * CSC 450 - Programming Languages
 */

/* Declarations */


%%

%class MyScanner /* Names the produced java file */
%function next /* Renames the yylex() function */
%type Token  /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}
%line

/* Patterns */

other = .

operator = [\+\-\*/]
symbol = [\;\{\}\@\(\)\=\,\:\"\|]
allSymbols = [\.]|{symbol}*

letter = [A-Za-z]
word = {letter}+
char = '{letter}'

number = [1-9][0-9]*|0
int = {number}+
float = {number}+.{number}+

stringContains = [ \t]*{letter}*|[ \t]*{number}*|[ \t]*{allSymbols}*
string = \"{stringContains}*\"

dataType = String|int|float|char|boolean

variableName = {letter}*|{number}*
variable = {letter}+{variableName}*

return = return
this = this|this.{variable}+
override = \@Override

whitespace = [\s]+
comment = [/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]|[/][/]{stringContains}*

%%

/* Lexical Rules */

{dataType} {
  switch (yytext()) {
    case "String" : return new Token(TokenType.DATATYPE_STRING, yytext());
    case "int" : return new Token(TokenType.DATATYPE_INT, yytext());
    case "float" : return new Token(TokenType.DATATYPE_FLOAT, yytext());
    case "char" : return new Token(TokenType.DATATYPE_CHAR, yytext());
    case "boolean" : return new Token(TokenType.DATATYPE_BOOLEAN, yytext());
  }
}

{variable} {
  return new Token(TokenType.VARIABLE, yytext());
}

{string} {
  return new Token(TokenType.STRING, yytext());
}

{char} {
  return new Token(TokenType.CHAR, yytext());
}

{int} {
  return new Token(TokenType.INT, yytext());
}

{float} {
  return new Token(TokenType.FLOAT, yytext());
}

{return} {
  return new Token(TokenType.RETURN, yytext());
}

{this} {
  return new Token(TokenType.THIS, yytext());
}

{override} {
  return new Token(TokenType.OVERRIDE, yytext());
}

{operator} {
  switch (yytext()) {
    case "+" : return new Token(TokenType.PLUS, yytext());
    case "-" : return new Token(TokenType.MINUS, yytext());
    case "*" : return new Token(TokenType.MULTIPLY, yytext());
    case "/" : return new Token(TokenType.DIVIDE, yytext());
  }
}

{symbol} {
  switch (yytext()) {
    case ";" : return new Token(TokenType.SEMICOLON, yytext());
    case "=" : return new Token(TokenType.EQUALS, yytext());
    case "(" : return new Token(TokenType.LEFTPARENTHESIS, yytext());
    case ")" : return new Token(TokenType.RIGHTPARENTHESIS, yytext());
    case "@" : return new Token(TokenType.ATSIGN, yytext());
    case "{" : return new Token(TokenType.LEFTCURLYBRACKET, yytext());
    case "}" : return new Token(TokenType.RIGHTCURLYBRACKET, yytext());
    case "," : return new Token(TokenType.COMMA, yytext());
  }
}

{comment} {
  /* Ignore Comments */
  //return new Token(TokenType.COMMENT, yytext());
}
            
{whitespace} {
  /* Ignore Whitespace */
}

{other} {
  throw new Error("Illegal character detected! Character is: " + yytext() + " on line: " + (yyline+1));
}